﻿using System;
using AutoMapper;
using CW9.DAL.Entities;
using CW9.Models;

namespace CW9
{
    public class MappingProfile: Profile
    {
        public MappingProfile()
        {
            CreateAdToAdModelMap();
            CreateAdCreateModelToAd();
            CreateAdToAdEditModel();
            CreateAdEditModelToAd();
        }

        public void CreateAdToAdModelMap()
        {
            CreateMap<Ad, AdModel>()
                .ForMember(target => target.DatePublished,
                    src => src.MapFrom(p => p.DatePublished.ToString("D")))
                .ForMember(target => target.OwnerName,
                    src => src.MapFrom(p => p.Owner.UserName))
                .ForMember(target=>target.Category,
                    src => src.MapFrom(p=> p.Category.Name))
                .ForMember(target => target.Images,
                    src => src.MapFrom(p => p.Images));;
        }

        public void CreateAdCreateModelToAd()
        {
            CreateMap<AdCreateModel, Ad>()
                .ForMember(target => target.Image,
                    src => src.Ignore())
                .ForMember(target => target.Images,
                    src => src.Ignore());
        }
        
        public void CreateAdEditModelToAd()
        {
            CreateMap<AdEditModel, Ad>()
                .ForMember(target => target.Image,
                    src => src.Ignore())
                .ForMember(target => target.Images,
                    src => src.Ignore());
        }
        
        public void CreateAdToAdEditModel()
        {
            CreateMap<Ad, AdEditModel>()
                .ForMember(target => target.Image,
                    src => src.Ignore())
                .ForMember(target => target.Images,
                    src => src.Ignore());
        }
    }
}