﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CW9.DAL.Entities;
using CW9.DAL.Repositories.Contracts;
using CW9.Models;
using CW9.Services.Contracts;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CW9.Services
{
    public class AdService: IAdService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IFileSaver _fileSaver;
        private readonly DbFileSaver _dbFileSaver;

        public AdService(IUnitOfWorkFactory unitOfWorkFactory, IFileSaver fileSaver, DbFileSaver dbFileSaver)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _fileSaver = fileSaver;
            _dbFileSaver = dbFileSaver;
        }
        
        public List<AdModel> GetAllAds(AdIndexModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var ads = unitOfWork.Ads.GetAllWithOwnersAndCategories();
                
                ads = ads
                    .BySearchKey(model.SearchKey)
                    .ByPriceFrom(model.PriceFrom)
                    .ByPriceTo(model.PriceTo)
                    .ByCategory(model.Category)
                    .ByImage(model.HasImage)
                    .SortByUpped();

                var models = Mapper.Map<List<AdModel>>(ads.ToList());

                return models;
            }
        }

        public AdModel GetById(int adId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var ad = unitOfWork.Ads.GetByIdWithOwnerAndCategory(adId);
         
                return Mapper.Map<AdModel>(ad);
            }
        }


        public void CreateAd(AdCreateModel model, int currentUserId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var ad = Mapper.Map<Ad>(model);
                ad.OwnerId = currentUserId;
                ad.Upped = DateTime.Now;
                ad.DatePublished = DateTime.Now;
                if (model.Image != null)
                {
                    _fileSaver.SaveFile(ad, model.Image);
                }
                Ad currentAd = unitOfWork.Ads.Create(ad);
                if (model.Images == null) return;
                foreach (var image in model.Images)
                {
                    var galleryImage = new Image()
                    {
                        ImageBytes = _dbFileSaver.GetImageBytes(image),
                        AdId = currentAd.Id
                    };
                    unitOfWork.Images.Create(galleryImage);
                }
            }
        }

        public AdCreateModel GetAdCreateModel()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var categories = unitOfWork.Categories.GetAll().ToList();

                return new AdCreateModel()
                {
                    CategoriesSelect = new SelectList(categories, nameof(Category.Id), nameof(Category.Name))
                };
            }
        }

        public void UpAd(int adId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var ad = unitOfWork.Ads.GetById(adId);
                ad.Upped = DateTime.Now;
                unitOfWork.Ads.Update(ad);
            }
        }

        public void EditAd(AdEditModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                Ad currentAd = unitOfWork.Ads.Update(Mapper.Map<Ad>(model));
                
                if (model.Image != null)
                {
                    _fileSaver.SaveFile(currentAd, model.Image);
                }
                
                if (model.Images == null) return;
                foreach (var image in model.Images)
                {
                    var galleryImage = new Image()
                    {
                        ImageBytes = _dbFileSaver.GetImageBytes(image),
                        AdId = currentAd.Id
                    };
                    unitOfWork.Images.Create(galleryImage);
                }
            }
        }

        public Ad GetAdById(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                return unitOfWork.Ads.GetByIdWithOwnerAndCategory(id);
            }
        }
    }
}