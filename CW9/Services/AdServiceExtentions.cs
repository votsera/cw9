﻿using System;
using System.Collections.Generic;
using System.Linq;
using CW9.DAL.Entities;

namespace CW9.Services
{
    public static class AdServiceExtentions
    {
        public static IEnumerable<Ad> BySearchKey(this IEnumerable<Ad> ads, string searchKey)
        {
            if (!string.IsNullOrWhiteSpace(searchKey))
                ads = ads.Where(r => r.Title.Contains(searchKey) || r.Content.Contains(searchKey));

            return ads;
        }

        public static IEnumerable<Ad> ByCategory(this IEnumerable<Ad> ads, string category)
        {
            
             if (!String.IsNullOrEmpty(category))
                 ads = ads.Where(r => r.Category.Name.Contains(category));
             return ads;
        }

        public static IEnumerable<Ad> ByPriceFrom(this IEnumerable<Ad> ads, int? priceFrom)
        {
            if (priceFrom.HasValue)
                ads = ads.Where(r => r.Price >= priceFrom.Value);

            return ads;
        }

        public static IEnumerable<Ad> ByPriceTo(this IEnumerable<Ad> ads, int? priceTo)
        {
            if (priceTo.HasValue)
                ads = ads.Where(r => r.Price <= priceTo.Value);

            return ads;
        }
        
        public static IEnumerable<Ad> ByImage(this IEnumerable<Ad> ads, bool hasImage)
        {
            if (hasImage == true)
                ads = ads.Where(r => r.Image != null);
            return ads;
        }
        public static IEnumerable<Ad> SortByUpped(this IEnumerable<Ad> ads)
        {
            return ads.ToList().OrderByDescending(a=>a.Upped);
        }
    }
}