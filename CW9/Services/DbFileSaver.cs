﻿using System.IO;
using CW9.DAL.Entities;
using CW9.Services.Contracts;
using Microsoft.AspNetCore.Http;

namespace CW9.Services
{
    public class DbFileSaver: IFileSaver
    {
        public void SaveFile(Ad ad, IFormFile formFile)
        {
            ad.Image = GetImageBytes(formFile);
        }

        public byte[] GetImageBytes(IFormFile formFile)
        {
            using (var binaryReader = new BinaryReader(formFile.OpenReadStream()))
            {
                return binaryReader.ReadBytes((int)formFile.Length);
            }
        }
    }
}