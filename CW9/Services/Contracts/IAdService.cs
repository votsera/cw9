﻿using System.Collections.Generic;
using CW9.DAL.Entities;
using CW9.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CW9.Services.Contracts
{
    public interface IAdService
    {
        List<AdModel> GetAllAds(AdIndexModel model);
        AdModel GetById(int adId);
        void CreateAd(AdCreateModel model, int currentUserId);
        AdCreateModel GetAdCreateModel();
        void UpAd(int adId);
        void EditAd(AdEditModel model);
        Ad GetAdById(int id);
    }
}