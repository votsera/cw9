﻿using CW9.DAL.Entities;
using Microsoft.AspNetCore.Http;

namespace CW9.Services.Contracts
{
    public interface IFileSaver
    {
        void SaveFile(Ad ad, IFormFile formFile);
    }
}