﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using CW9.DAL.Entities;
using CW9.Models;
using CW9.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CW9.Controllers
{
    public class AdController: Controller
    {
        private readonly IAdService _adService;
        private readonly UserManager<User> _userManager;

        public AdController(IAdService adService, UserManager<User> userManager)
        {
            _adService = adService;
            _userManager = userManager;
        }
        
        public IActionResult Index(AdIndexModel model)
        {
            try
            {
                var adModels = _adService.GetAllAds(model);
                
                model.Ads = adModels;

                return View(model);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [Authorize]
        public IActionResult Create()
        {
            var model = _adService.GetAdCreateModel();
            return View(model);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> CreateAd(AdCreateModel model)
        {
            try
            {
                User currentUser = await _userManager.GetUserAsync(User);

                 _adService.CreateAd(model, currentUser.Id);

                 return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [Authorize]
        [HttpGet]
        public IActionResult Details(int adId)
        {
            var adModel = _adService.GetById(adId);

            return View(adModel);
        }
        
        [Route("Edit/{id?}")]
        public IActionResult Edit(int? id)
        {
            if (!id.HasValue)
            {
                ViewBag.BadRequestMessage = "Product id cannot be NULL";
                return View("Error");
            }

            var adEditModel = _adService.GetAdById(id.Value);

            return View(adEditModel);
        }

        [HttpPost]
        public IActionResult EditProduct(AdEditModel model)
        {
            try
            {
                _adService.EditAd(model);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
        
        
        [Authorize]
        [HttpPost]
        public IActionResult UpAjax(int adId)
        {
            try
            {
                _adService.UpAd(adId);

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }
    }
}