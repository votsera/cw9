using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using AutoMapper;
using CW9.DAL;
using CW9.DAL.Entities;
using CW9.DAL.EntitiesConfiguration;
using CW9.DAL.EntitiesConfiguration.Contracts;
using CW9.DAL.Repositories;
using CW9.DAL.Repositories.Contracts;
using CW9.Services;
using CW9.Services.Contracts;
using Microsoft.EntityFrameworkCore;


namespace CW9
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            string connectionString = Configuration.GetConnectionString("MainConnectionString");

            var optionsBuilder = new DbContextOptionsBuilder();
            optionsBuilder.UseSqlServer(connectionString);

            services.AddScoped<IEntityConfigurationContainer>(sp => new EntityConfigurationContainer());

            services.AddSingleton<IAppDbContextFactory>(
                sp => new AppDbContextFactory(
                    optionsBuilder.Options,
                    new EntityConfigurationContainer()
                ));

            services.AddDbContext<AppDbContext>(options =>
            {
                options.UseSqlServer(connectionString);
            });
            services.AddSingleton<IUnitOfWorkFactory, UnitOfWorkFactory>();

            services.AddTransient<IAdService, AdService>();
            services.AddTransient<IFileSaver, DbFileSaver>();
            services.AddTransient<DbFileSaver>();
            
            services.AddDefaultIdentity<User>(
                    options =>
                    {
                        options.SignIn.RequireConfirmedAccount = false;
                        options.Password.RequireDigit = false;
                        options.Password.RequireLowercase = false;
                        options.Password.RequireNonAlphanumeric = false;
                        options.Password.RequireUppercase = false;
                        options.Password.RequiredLength = 3;
                    })
                .AddEntityFrameworkStores<AppDbContext>();
            
            services.AddControllersWithViews();
            services.AddRazorPages();

            Mapper.Initialize(config => config.AddProfile(new MappingProfile()));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Ad}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });
        }
    }
}