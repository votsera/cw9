﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CW9.DAL.Entities;

namespace CW9.Models
{
    public class AdModel
    {
        public int Id { get; set; }
        public string DatePublished { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int Price { get; set; }
        public string OwnerContact { get; set; }
        public string OwnerName { get; set; }
        public string Category { get; set; }
        public DateTime Upped { get; set; }
        public byte[] Image { get; set; }
        public List<Image> Images { get; set; }
    }
}