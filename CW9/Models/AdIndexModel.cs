﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CW9.Models
{
    public class AdIndexModel
    {
        public string SearchKey { get; set; }
        public string Category { get; set; }
        public int? PriceFrom { get; set; }
        public int? PriceTo { get; set; }
        public bool HasImage { get;  set; }
        public List<AdModel> Ads { get; set; }
        public int? Page { get; set; }
    }
}