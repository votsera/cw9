﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CW9.Models
{
    public class AdEditModel
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Title")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Content")]
        public string Content { get; set; }
        
        [Display(Name = "Category")]
        public int CategoryId { get; set; }
        public SelectList CategoriesSelect { get; set; }
        
        [Required]
        [Display(Name = "Contact")]
        public string OwnerContact { get; set; }
        
        [Required]
        [Display(Name = "Price")]
        public int Price { get; set; }
        
        [Display(Name = "Main image")]
        public IFormFile Image { get; set; }
        
        [Display(Name = "Image gallery")]
        public IFormFileCollection Images { get; set; }
    }
}