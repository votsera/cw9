﻿using System.Linq;
using CW9.DAL.Entities;
using CW9.DAL.EntitiesConfiguration.Contracts;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace CW9.DAL
{
    
       public class AppDbContext : IdentityDbContext<User, Role, int>
       {
           private readonly IEntityConfigurationContainer _entityConfigurationsContainer;
           public DbSet<Ad> Ads { get; set; }
           public DbSet<Category> Categories { get; set; }
           public DbSet<Image> Images { get; set; }
           public AppDbContext(
               DbContextOptions options,
               IEntityConfigurationContainer entityConfigurationsContainer) : base(options)
           {
               _entityConfigurationsContainer = entityConfigurationsContainer;
           }
           protected override void OnModelCreating(ModelBuilder builder)
           {
               base.OnModelCreating(builder);
               builder.Entity(_entityConfigurationsContainer.AdConfiguration.ProvideConfigurationAction());
               builder.Entity(_entityConfigurationsContainer.ImageConfiguration.ProvideConfigurationAction());
               builder.Entity(_entityConfigurationsContainer.CategoryConfiguration.ProvideConfigurationAction());
               DisableOneToManyCascadeDelete(builder);
           }
           private void DisableOneToManyCascadeDelete(ModelBuilder builder)
           {
               foreach (var relation in builder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
               {
                   relation.DeleteBehavior = DeleteBehavior.Restrict;
               }
           }
       }
    
}