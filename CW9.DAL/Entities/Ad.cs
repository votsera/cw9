﻿using System;
using System.Collections.Generic;
using System.Net.Mime;

namespace CW9.DAL.Entities
{
    public class Ad: IEntity
    {
        public int Id { get; set; }
        public DateTime DatePublished { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int Price { get; set; }
        public int OwnerId { get; set; }
        public string OwnerContact { get; set; }
        public User Owner { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public DateTime Upped { get; set; }
        public byte[] Image { get; set; }
        public ICollection<Image> Images { get; set; }
    }
}