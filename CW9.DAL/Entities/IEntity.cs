﻿namespace CW9.DAL.Entities
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}