﻿namespace CW9.DAL.Entities
{
    public class Image: IEntity
    {
        public int Id { get; set; }
        public byte[] ImageBytes { get; set; }
        public int AdId { get; set; }
        public Ad Ad { get; set; }

    }
}