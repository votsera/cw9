﻿using Microsoft.AspNetCore.Identity;

namespace CW9.DAL.Entities
{
    public class Role : IdentityRole<int>, IEntity
    {
    }
}