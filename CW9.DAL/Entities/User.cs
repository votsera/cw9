﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace CW9.DAL.Entities
{
    public class User : IdentityUser<int>, IEntity
    {
        public ICollection<Ad> Ads { get; set; }
    }
}