﻿namespace CW9.DAL
{
    public interface IAppDbContextFactory
    {
        AppDbContext Create();
    }
}