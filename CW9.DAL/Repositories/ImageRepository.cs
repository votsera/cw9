﻿using CW9.DAL.Entities;
using CW9.DAL.Repositories.Contracts;

namespace CW9.DAL.Repositories
{
    public class ImageRepository: Repository<Image>, IImageRepository
    {
        public ImageRepository(AppDbContext context) : base(context)
        {
            entities = context.Images;
        }
    }
}