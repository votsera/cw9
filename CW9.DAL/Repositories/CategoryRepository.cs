﻿using CW9.DAL.Entities;
using CW9.DAL.Repositories.Contracts;

namespace CW9.DAL.Repositories
{
    public class CategoryRepository: Repository<Category>, ICategoryRepository
    {
        public CategoryRepository(AppDbContext context) : base(context)
        {
            entities = context.Categories;
        }
    }
}