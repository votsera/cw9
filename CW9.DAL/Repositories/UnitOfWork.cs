﻿using System;
using CW9.DAL.Repositories.Contracts;

namespace CW9.DAL.Repositories
{
    public class UnitOfWork: IDisposable
    {
        private readonly AppDbContext _context;
        private bool _disposed;

        public IAdRepository Ads { get; set; }
        public ICategoryRepository Categories { get; set; }
        public IImageRepository Images { get; set; }

        public UnitOfWork(AppDbContext context)
        {
            _context = context;

            Ads = new AdRepository(context);
            Categories = new CategoryRepository(context);
            Images = new ImageRepository(context);
        }

        #region Disposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
                _context.Dispose();

            _disposed = true;
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }
        #endregion
    }
}