﻿using System.Collections.Generic;
using System.Linq;
using CW9.DAL.Entities;
using CW9.DAL.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;

namespace CW9.DAL.Repositories
{
    public class AdRepository: Repository<Ad>, IAdRepository
    {
        public AdRepository(AppDbContext context) : base(context)
        {
            entities = context.Ads;
        }

        public IEnumerable<Ad> GetAllWithOwnersAndCategories()
        {
            return entities
                .Include(a => a.Owner)
                .Include(a => a.Category)
                .ToList();
        }

        public Ad GetByIdWithOwnerAndCategory(int id)
        {
            return entities
                .Include(e => e.Owner)
                .Include(e => e.Images)
                .Include(e => e.Category)
                .FirstOrDefault(c => c.Id == id);
        }
    }
}