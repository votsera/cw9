﻿using CW9.DAL.Repositories.Contracts;

namespace CW9.DAL.Repositories
{
    public class UnitOfWorkFactory: IUnitOfWorkFactory
    {
        private readonly IAppDbContextFactory _applicationDbContextFactory;

        public UnitOfWorkFactory(IAppDbContextFactory applicationDbContextFactory)
        {
            _applicationDbContextFactory = applicationDbContextFactory;
        }

        public UnitOfWork Create()
        {
            return new UnitOfWork(_applicationDbContextFactory.Create());
        }
    }
}