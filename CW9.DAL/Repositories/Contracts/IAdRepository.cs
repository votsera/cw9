﻿using System.Collections.Generic;
using CW9.DAL.Entities;

namespace CW9.DAL.Repositories.Contracts
{
    public interface IAdRepository: IRepository<Ad>
    {
        IEnumerable<Ad> GetAllWithOwnersAndCategories();
        Ad GetByIdWithOwnerAndCategory(int id);
    }
}
