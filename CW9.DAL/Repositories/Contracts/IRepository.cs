﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CW9.DAL.Entities;

namespace CW9.DAL.Repositories.Contracts
{
    public interface IRepository<T> where T : class, IEntity
    {
        T Create(T entity);

        Task<T> CreateAsync(T entity);

        T GetById(int id);

        IEnumerable<T> GetAll();

        T Update(T entity);

        void Remove(T entity);
    }
}