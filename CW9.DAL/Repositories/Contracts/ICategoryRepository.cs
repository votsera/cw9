﻿using CW9.DAL.Entities;

namespace CW9.DAL.Repositories.Contracts
{
    public interface ICategoryRepository: IRepository<Category>
    {
        
    }
}