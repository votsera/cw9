﻿using System;
using Microsoft.EntityFrameworkCore;
using CW9.DAL.EntitiesConfiguration.Contracts;

namespace CW9.DAL
{
    public class AppDbContextFactory : IAppDbContextFactory
    {
        private readonly DbContextOptions _options;
        private readonly IEntityConfigurationContainer _entityConfigurationsContainer;

        public AppDbContextFactory(
            DbContextOptions options,
            IEntityConfigurationContainer entityConfigurationsContainer)
        {
            if (options == null)
                throw new ArgumentNullException(nameof(options));
            if (entityConfigurationsContainer == null)
                throw new ArgumentNullException(nameof(entityConfigurationsContainer));

            _options = options;
            _entityConfigurationsContainer = entityConfigurationsContainer;
        }

        public AppDbContext Create()
        {
            return new AppDbContext(_options, _entityConfigurationsContainer);
        }
    }
}