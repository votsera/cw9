﻿using CW9.DAL.Entities;
using CW9.DAL.EntitiesConfiguration.Contracts;

namespace CW9.DAL.EntitiesConfiguration
{
    public class EntityConfigurationContainer: IEntityConfigurationContainer
    {
        public IEntityConfiguration<Ad> AdConfiguration { get; }
        public IEntityConfiguration<Category> CategoryConfiguration { get; }
        public IEntityConfiguration<Image> ImageConfiguration { get; }

        public EntityConfigurationContainer()
        {
            AdConfiguration = new AdConfiguration();
            CategoryConfiguration = new CategoryConfiguration();
            ImageConfiguration = new ImageConfiguration();
        }
    }
}