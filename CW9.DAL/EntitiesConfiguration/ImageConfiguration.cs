﻿using CW9.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CW9.DAL.EntitiesConfiguration
{
    public class ImageConfiguration : BaseEntityConfiguration<Image>
        {
            protected override void ConfigureForeignKeys(EntityTypeBuilder<Image> builder)
            {
                builder
                    .HasOne(i => i.Ad)
                    .WithMany(a => a.Images)
                    .HasForeignKey(i => i.AdId)
                    .IsRequired();
            }
        }
}