﻿using CW9.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CW9.DAL.EntitiesConfiguration
{
    public class CategoryConfiguration : BaseEntityConfiguration<Category>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Category> builder)
        {
            builder
                .Property(b => b.Name)
                .HasMaxLength(100)
                .IsRequired();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Category> builder)
        {
            builder
                .HasMany(c => c.Ads)
                .WithOne(a => a.Category)
                .HasForeignKey(a => a.CategoryId)
                .IsRequired();
        }
    }
}