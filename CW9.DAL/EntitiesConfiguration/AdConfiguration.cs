﻿using CW9.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CW9.DAL.EntitiesConfiguration
{
    public class AdConfiguration: BaseEntityConfiguration<Ad>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Ad> builder)
        {
            builder
                .Property(b => b.Title)
                .HasMaxLength(100)
                .IsRequired();

            builder
                .Property(b => b.Content)
                .HasMaxLength(int.MaxValue)
                .IsRequired();
            
            builder
                .Property(b => b.Price)
                .HasMaxLength(int.MaxValue)
                .IsRequired();
            
            builder
                .Property(b => b.OwnerContact)
                .HasMaxLength(10)
                .IsRequired();

            builder
                .Property(b => b.DatePublished)
                .IsRequired();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Ad> builder)
        {
            builder
                .HasOne(a => a.Owner)
                .WithMany(u => u.Ads)
                .HasForeignKey(a => a.OwnerId)
                .IsRequired();

            builder
                .HasOne(a => a.Category)
                .WithMany(c => c.Ads)
                .HasForeignKey(a => a.CategoryId);
            
            builder
                .HasMany(a => a.Images)
                .WithOne(i => i.Ad)
                .HasForeignKey(i => i.AdId);
        }
    }
}