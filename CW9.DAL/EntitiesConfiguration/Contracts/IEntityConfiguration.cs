﻿using System;
using CW9.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CW9.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfiguration<T> where T : class, IEntity
    {
        Action<EntityTypeBuilder<T>> ProvideConfigurationAction();
    }
}