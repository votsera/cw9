﻿using CW9.DAL.Entities;

namespace CW9.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfigurationContainer
    {
        IEntityConfiguration<Ad> AdConfiguration { get; }
        IEntityConfiguration<Category> CategoryConfiguration { get; }
        IEntityConfiguration<Image> ImageConfiguration { get; }
    }
}